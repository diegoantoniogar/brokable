<?php

/* themes/contrib/bootstrap/templates/paragraph/paragraph--icono_titulo_descripcion.html.twig */
class __TwigTemplate_56bd66fc6f66311748621e7884e7d36288b3676fd83990aada9afb529e21d709 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 1);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        $context["nivel"] = $this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_nivel", array()), "value", array());
        // line 2
        $context["h_level"] = (((($context["nivel"] ?? null) == 1)) ? (3) : (4));
        // line 3
        echo "
<div class=\"container icono_titulo_descripcion nivel-";
        // line 4
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["nivel"] ?? null), "html", null, true));
        echo "\">
  <div class=\"row\">
    <div class=\"col-md-12 icono_titulo_descripcion-encabezado\">
      <div class=\"icono_titulo_descripcion-icono\">
        ";
        // line 8
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_icono", array()), "html", null, true));
        echo "
      </div>
      <div class=\"icono_titulo_descripcion-titulo\">
        <h";
        // line 11
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["h_level"] ?? null), "html", null, true));
        echo ">
          ";
        // line 12
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_titulo", array()), "html", null, true));
        echo "
        </h";
        // line 13
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["h_level"] ?? null), "html", null, true));
        echo ">
      </div>
    </div>

    <div class=\"col-md-12 icono_titulo_descripcion-texto\">
      ";
        // line 18
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_descripcion", array()), "html", null, true));
        echo "
    </div>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/contrib/bootstrap/templates/paragraph/paragraph--icono_titulo_descripcion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 18,  71 => 13,  67 => 12,  63 => 11,  57 => 8,  50 => 4,  47 => 3,  45 => 2,  43 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/contrib/bootstrap/templates/paragraph/paragraph--icono_titulo_descripcion.html.twig", "/homepages/20/d730456019/htdocs/clickandbuilds/Drupal/Brokable/themes/contrib/bootstrap/templates/paragraph/paragraph--icono_titulo_descripcion.html.twig");
    }
}
