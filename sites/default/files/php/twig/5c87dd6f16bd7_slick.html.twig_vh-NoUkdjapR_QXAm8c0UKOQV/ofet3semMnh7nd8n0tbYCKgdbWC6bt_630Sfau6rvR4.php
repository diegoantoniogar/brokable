<?php

/* modules/contrib/slick/templates/slick.html.twig */
class __TwigTemplate_78c4457bb00a6b32399e8cfd0acf126b64537e4077cb7dd8a4b01b1575f49e95 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'slick_content' => array($this, 'block_slick_content'),
            'slick_arrow' => array($this, 'block_slick_arrow'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 31, "if" => 57, "block" => 61, "for" => 62);
        $filters = array("join" => 35, "clean_class" => 36, "raw" => 71, "striptags" => 71);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block', 'for'),
                array('join', 'clean_class', 'raw', 'striptags'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 31
        $context["classes"] = array(0 => (($this->getAttribute(        // line 32
($context["settings"] ?? null), "unslick", array())) ? ("unslick") : ("")), 1 => ((((        // line 33
($context["display"] ?? null) == "main") && $this->getAttribute(($context["settings"] ?? null), "blazy", array()))) ? ("blazy") : ("")), 2 => (($this->getAttribute(        // line 34
($context["settings"] ?? null), "vertical", array())) ? ("slick--vertical") : ("")), 3 => (($this->getAttribute($this->getAttribute(        // line 35
($context["settings"] ?? null), "attributes", array()), "class", array())) ? (twig_join_filter($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "attributes", array()), "class", array()), " ")) : ("")), 4 => (($this->getAttribute(        // line 36
($context["settings"] ?? null), "skin", array())) ? (("slick--skin--" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(($context["settings"] ?? null), "skin", array())))) : ("")), 5 => ((twig_in_filter("boxed", $this->getAttribute(        // line 37
($context["settings"] ?? null), "skin", array()))) ? ("slick--skin--boxed") : ("")), 6 => ((twig_in_filter("split", $this->getAttribute(        // line 38
($context["settings"] ?? null), "skin", array()))) ? ("slick--skin--split") : ("")), 7 => (($this->getAttribute(        // line 39
($context["settings"] ?? null), "optionset", array())) ? (("slick--optionset--" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(($context["settings"] ?? null), "optionset", array())))) : ("")), 8 => ((        // line 40
array_key_exists("arrow_down_attributes", $context)) ? ("slick--has-arrow-down") : ("")), 9 => (($this->getAttribute(        // line 41
($context["settings"] ?? null), "asNavFor", array())) ? (("slick--" . \Drupal\Component\Utility\Html::getClass(($context["display"] ?? null)))) : ("")), 10 => ((($this->getAttribute(        // line 42
($context["settings"] ?? null), "slidesToShow", array()) > 1)) ? ("slick--multiple-view") : ("")), 11 => ((($this->getAttribute(        // line 43
($context["settings"] ?? null), "count", array()) <= $this->getAttribute(($context["settings"] ?? null), "slidesToShow", array()))) ? ("slick--less") : ("")), 12 => ((((        // line 44
($context["display"] ?? null) == "main") && $this->getAttribute(($context["settings"] ?? null), "media_switch", array()))) ? (("slick--" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(($context["settings"] ?? null), "media_switch", array())))) : ("")), 13 => ((((        // line 45
($context["display"] ?? null) == "thumbnail") && $this->getAttribute(($context["settings"] ?? null), "thumbnail_caption", array()))) ? ("slick--has-caption") : ("")));
        // line 49
        $context["arrow_classes"] = array(0 => "slick__arrow", 1 => (($this->getAttribute(        // line 51
($context["settings"] ?? null), "vertical", array())) ? ("slick__arrow--v") : ("")), 2 => (($this->getAttribute(        // line 52
($context["settings"] ?? null), "skin_arrows", array())) ? (("slick__arrow--" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(($context["settings"] ?? null), "skin_arrows", array())))) : ("")));
        // line 55
        echo "
<div";
        // line 56
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo ">
  ";
        // line 57
        if ( !$this->getAttribute(($context["settings"] ?? null), "unslick", array())) {
            // line 58
            echo "    <div";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content_attributes"] ?? null), "addClass", array(0 => "slick__slider"), "method"), "html", null, true));
            echo ">
  ";
        }
        // line 60
        echo "
  ";
        // line 61
        $this->displayBlock('slick_content', $context, $blocks);
        // line 66
        echo "
  ";
        // line 67
        if ( !$this->getAttribute(($context["settings"] ?? null), "unslick", array())) {
            // line 68
            echo "    </div>
    ";
            // line 69
            $this->displayBlock('slick_arrow', $context, $blocks);
            // line 81
            echo "  ";
        }
        // line 82
        echo "</div>
";
    }

    // line 61
    public function block_slick_content($context, array $blocks = array())
    {
        // line 62
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 63
            echo "      ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $context["item"], "html", null, true));
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "  ";
    }

    // line 69
    public function block_slick_arrow($context, array $blocks = array())
    {
        // line 70
        echo "      <nav";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["arrow_attributes"] ?? null), "addClass", array(0 => ($context["arrow_classes"] ?? null)), "method"), "html", null, true));
        echo ">
        ";
        // line 71
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(strip_tags($this->getAttribute(($context["settings"] ?? null), "prevArrow", array()), "<a><em><span><strong><button><div>")));
        echo "
        ";
        // line 72
        if (array_key_exists("arrow_down_attributes", $context)) {
            // line 73
            echo "          <button";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["arrow_down_attributes"] ?? null), "addClass", array(0 => "slick-down"), "method"), "setAttribute", array(0 => "type", 1 => "button"), "method"), "setAttribute", array(0 => "data-target", 1 => $this->getAttribute(            // line 75
($context["settings"] ?? null), "downArrowTarget", array())), "method"), "setAttribute", array(0 => "data-offset", 1 => $this->getAttribute(            // line 76
($context["settings"] ?? null), "downArrowOffset", array())), "method"), "html", null, true));
            echo "></button>
        ";
        }
        // line 78
        echo "        ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(strip_tags($this->getAttribute(($context["settings"] ?? null), "nextArrow", array()), "<a><em><span><strong><button><div>")));
        echo "
      </nav>
    ";
    }

    public function getTemplateName()
    {
        return "modules/contrib/slick/templates/slick.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 78,  141 => 76,  140 => 75,  138 => 73,  136 => 72,  132 => 71,  127 => 70,  124 => 69,  120 => 65,  111 => 63,  106 => 62,  103 => 61,  98 => 82,  95 => 81,  93 => 69,  90 => 68,  88 => 67,  85 => 66,  83 => 61,  80 => 60,  74 => 58,  72 => 57,  68 => 56,  65 => 55,  63 => 52,  62 => 51,  61 => 49,  59 => 45,  58 => 44,  57 => 43,  56 => 42,  55 => 41,  54 => 40,  53 => 39,  52 => 38,  51 => 37,  50 => 36,  49 => 35,  48 => 34,  47 => 33,  46 => 32,  45 => 31,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modules/contrib/slick/templates/slick.html.twig", "/homepages/20/d730456019/htdocs/clickandbuilds/Drupal/Brokable/modules/contrib/slick/templates/slick.html.twig");
    }
}
