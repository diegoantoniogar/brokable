<?php

/* modules/contrib/blazy/templates/blazy.html.twig */
class __TwigTemplate_aef4a77e7bcd8e13fadb4fef19273be840946cc8a0e6c47e5e13f3a7085354fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'blazy_player' => array($this, 'block_blazy_player'),
            'blazy_media' => array($this, 'block_blazy_media'),
            'blazy_content' => array($this, 'block_blazy_content'),
            'blazy_caption' => array($this, 'block_blazy_caption'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 27, "if" => 47, "block" => 48, "for" => 90);
        $filters = array("clean_class" => 31, "join" => 36);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block', 'for'),
                array('clean_class', 'join'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 27
        $context["classes"] = array(0 => "media", 1 => (($this->getAttribute(        // line 29
($context["settings"] ?? null), "namespace", array())) ? (("media--" . $this->getAttribute(($context["settings"] ?? null), "namespace", array()))) : ("")), 2 => (($this->getAttribute(        // line 30
($context["settings"] ?? null), "lazy", array())) ? ("media--loading") : ("")), 3 => (($this->getAttribute(        // line 31
($context["settings"] ?? null), "media_switch", array())) ? (("media--switch media--switch--" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(($context["settings"] ?? null), "media_switch", array())))) : ("")), 4 => (($this->getAttribute(        // line 32
($context["settings"] ?? null), "player", array())) ? ("media--player") : ("")), 5 => (($this->getAttribute(        // line 33
($context["settings"] ?? null), "ratio", array())) ? (("media--ratio media--ratio--" . $this->getAttribute(($context["settings"] ?? null), "ratio", array()))) : ("")), 6 => (($this->getAttribute(        // line 34
($context["settings"] ?? null), "responsive_image_style_id", array())) ? ("media--responsive") : ("")), 7 => (($this->getAttribute(        // line 35
($context["settings"] ?? null), "type", array())) ? (("media--" . $this->getAttribute(($context["settings"] ?? null), "type", array()))) : ("")), 8 => (($this->getAttribute(        // line 36
($context["settings"] ?? null), "classes", array())) ? (\Drupal\Component\Utility\Html::getClass(twig_join_filter($this->getAttribute(($context["settings"] ?? null), "classes", array()), " "))) : ("")));
        // line 40
        $context["iframe_classes"] = array(0 => "media__iframe", 1 => (($this->getAttribute(        // line 42
($context["settings"] ?? null), "ratio", array())) ? ("media__element") : ("")));
        // line 45
        echo "
";
        // line 46
        ob_start();
        // line 47
        echo "  ";
        if ($this->getAttribute(($context["settings"] ?? null), "player", array())) {
            // line 48
            echo "    ";
            $this->displayBlock('blazy_player', $context, $blocks);
            // line 55
            echo "  ";
        }
        $context["player"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 57
        echo "
";
        // line 58
        ob_start();
        // line 59
        echo "  ";
        $this->displayBlock('blazy_media', $context, $blocks);
        $context["media"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 67
        echo "
";
        // line 68
        ob_start();
        // line 69
        echo "  ";
        $this->displayBlock('blazy_content', $context, $blocks);
        // line 86
        echo "
  ";
        // line 87
        if ((($context["captions"] ?? null) && $this->getAttribute(($context["captions"] ?? null), "inline", array(), "any", true, true))) {
            // line 88
            echo "    ";
            $this->displayBlock('blazy_caption', $context, $blocks);
            // line 97
            echo "  ";
        }
        $context["blazy"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 99
        echo "
";
        // line 100
        if (($context["wrapper_attributes"] ?? null)) {
            // line 101
            echo "  <div";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["wrapper_attributes"] ?? null), "html", null, true));
            echo ">
    ";
            // line 102
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["blazy"] ?? null), "html", null, true));
            echo "
  </div>
";
        } else {
            // line 105
            echo "  ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["blazy"] ?? null), "html", null, true));
            echo "
";
        }
    }

    // line 48
    public function block_blazy_player($context, array $blocks = array())
    {
        // line 49
        echo "      <iframe";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["iframe_attributes"] ?? null), "addClass", array(0 => ($context["iframe_classes"] ?? null)), "method"), "html", null, true));
        echo " allowfullscreen></iframe>
      ";
        // line 50
        if (($this->getAttribute(($context["settings"] ?? null), "media_switch", array()) && $this->getAttribute(($context["settings"] ?? null), "autoplay_url", array()))) {
            // line 51
            echo "        <span class=\"media__icon media__icon--close\"></span>
        <span class=\"media__icon media__icon--play\" data-url=\"";
            // line 52
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["settings"] ?? null), "embed_url", array()), "html", null, true));
            echo "\" data-autoplay=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["settings"] ?? null), "autoplay_url", array()), "html", null, true));
            echo "\"></span>
      ";
        }
        // line 54
        echo "    ";
    }

    // line 59
    public function block_blazy_media($context, array $blocks = array())
    {
        // line 60
        echo "    <div";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo ">
      ";
        // line 61
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["image"] ?? null), "html", null, true));
        echo "
      ";
        // line 62
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["player"] ?? null), "html", null, true));
        echo "
      ";
        // line 63
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["settings"] ?? null), "icon", array()), "html", null, true));
        echo "
    </div>
  ";
    }

    // line 69
    public function block_blazy_content($context, array $blocks = array())
    {
        // line 70
        echo "    ";
        if (($context["media_attributes"] ?? null)) {
            echo "<div";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["media_attributes"] ?? null), "html", null, true));
            echo ">";
        }
        // line 71
        echo "      ";
        if ((($context["url"] ?? null) &&  !$this->getAttribute(($context["settings"] ?? null), "player", array()))) {
            // line 72
            echo "        <a href=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["url"] ?? null), "html", null, true));
            echo "\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["url_attributes"] ?? null), "html", null, true));
            echo ">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["media"] ?? null), "html", null, true));
            echo "</a>

        ";
            // line 75
            echo "        ";
            if ((($context["captions"] ?? null) &&  !twig_test_empty($this->getAttribute(($context["captions"] ?? null), "lightbox", array())))) {
                // line 76
                echo "          <div class=\"litebox-caption visually-hidden\">";
                // line 77
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["captions"] ?? null), "lightbox", array()), "html", null, true));
                // line 78
                echo "</div>
        ";
            }
            // line 80
            echo "
      ";
        } else {
            // line 82
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["media"] ?? null), "html", null, true));
        }
        // line 84
        echo "    ";
        if (($context["media_attributes"] ?? null)) {
            echo "</div>";
        }
        // line 85
        echo "  ";
    }

    // line 88
    public function block_blazy_caption($context, array $blocks = array())
    {
        // line 89
        echo "      <div";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["caption_attributes"] ?? null), "html", null, true));
        echo ">
        ";
        // line 90
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["captions"] ?? null), "inline", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["caption"]) {
            // line 91
            echo "          ";
            if ($this->getAttribute($context["caption"], "content", array())) {
                // line 92
                echo "            <";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["caption"], "tag", array()), "html", null, true));
                echo " ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["caption"], "attributes", array()), "html", null, true));
                echo ">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["caption"], "content", array()), "html", null, true));
                echo "</";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["caption"], "tag", array()), "html", null, true));
                echo ">
          ";
            }
            // line 94
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['caption'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "      </div>
    ";
    }

    public function getTemplateName()
    {
        return "modules/contrib/blazy/templates/blazy.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  258 => 95,  252 => 94,  240 => 92,  237 => 91,  233 => 90,  228 => 89,  225 => 88,  221 => 85,  216 => 84,  213 => 82,  209 => 80,  205 => 78,  203 => 77,  201 => 76,  198 => 75,  188 => 72,  185 => 71,  178 => 70,  175 => 69,  168 => 63,  164 => 62,  160 => 61,  155 => 60,  152 => 59,  148 => 54,  141 => 52,  138 => 51,  136 => 50,  131 => 49,  128 => 48,  120 => 105,  114 => 102,  109 => 101,  107 => 100,  104 => 99,  100 => 97,  97 => 88,  95 => 87,  92 => 86,  89 => 69,  87 => 68,  84 => 67,  80 => 59,  78 => 58,  75 => 57,  71 => 55,  68 => 48,  65 => 47,  63 => 46,  60 => 45,  58 => 42,  57 => 40,  55 => 36,  54 => 35,  53 => 34,  52 => 33,  51 => 32,  50 => 31,  49 => 30,  48 => 29,  47 => 27,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modules/contrib/blazy/templates/blazy.html.twig", "/homepages/20/d730456019/htdocs/clickandbuilds/Drupal/Brokable/modules/contrib/blazy/templates/blazy.html.twig");
    }
}
